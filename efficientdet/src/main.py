from effdet import get_efficientdet_config, EfficientDet, DetBenchTrain
from effdet import create_model, unwrap_bench, create_loader, create_dataset, create_evaluator, create_model_from_config
from effdet.efficientdet import HeadNet
from torch.utils.data import DataLoader
import torch
from torch.utils.data.sampler import SequentialSampler, RandomSampler
from config import Config as config
from train import Fitter
from dataset import TrainDataset
from transforms import get_valid_transforms, get_train_transforms
import pandas as pd
import torchvision
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor
import os
import random
import numpy as np
from tqdm.contrib.telegram import tqdm, trange
import sys
# sys.path.append("/content/drive/MyDrive/japan_detection/src/")


SEED = config.seed

def seed_everything(seed):
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False

seed_everything(SEED)



df = pd.read_csv(config.df_path)

df = pd.read_csv('../input/normalised_wbf_folds_train.csv')
df = df[df['class_id']!=14].reset_index(drop=True)



# train_dataset = TrainDataset(
#     image_path=config.train_images_path,
#     marking=df[df['fold_{config.fold_number}']!='train'],
#     transforms=get_valid_transforms(),
#     split='train',
#     im_size=config.im_size
# )

# validation_dataset = TrainDataset(
#     image_path=config.train_images_path,
#     marking=df[df['fold_{config.fold_number}']!='valid'],
#     transforms=get_valid_transforms(),
#     split='test',
#     im_size=config.im_size
# )

# def collate_fn(batch):
#     return tuple(zip(*batch))

# def run_training():
#     device = torch.device('cuda:0')
#     net.to(device)

#     train_loader = torch.utils.data.DataLoader(
#         train_dataset,
#         batch_size=config.batch_size,
#         sampler=RandomSampler(train_dataset),
#         pin_memory=False,
#         drop_last=True,
#         num_workers=config.num_workers,
#         collate_fn=collate_fn,
#     )
#     val_loader = torch.utils.data.DataLoader(
#         validation_dataset, 
#         batch_size=config.batch_size,
#         num_workers=config.num_workers,
#         shuffle=False,
#         sampler=SequentialSampler(validation_dataset),
#         pin_memory=False,
#         collate_fn=collate_fn,
#     )

#     fitter = Fitter(model=net, device=device, config=config)
#     try:
#         fitter.load(config.checkpoint_path)
#         print('checkpoint:', config.checkpoint_path)
#     except:
#         print('checkpoint hasnt been loaded')
#     fitter.fit(train_loader, val_loader)




checkpoint_path = config.checkpoint_path
# checkpoint_path = None

device = torch.device('cuda:0' if torch.cuda.is_available() else "cpu")


def collate_fn(batch):
    batch = list(filter(lambda x: x is not None, batch))
    
    return tuple(zip(*batch))

# def collate_fn(batch):
#     return tuple(zip(*batch))

fold_history = []


# for val_fold in range(config.kfold):
for val_fold in [3]:
    print(f'Fold {val_fold+1}/{config.kfold}')
    
    train_dataset = TrainDataset(
            image_path=config.train_images_path,
            marking=df[df[f'fold_{val_fold}']=='train'],
            transforms=get_train_transforms(),
            split='train',
            im_size=1
    )

    validation_dataset = TrainDataset(
            image_path=config.train_images_path,
            marking=df[df[f'fold_{val_fold}']=='valid'],
            transforms=get_valid_transforms(),
            split='test',
            im_size=1
    )

    train_loader = torch.utils.data.DataLoader(
            train_dataset,
            batch_size=config.batch_size,
            sampler=RandomSampler(train_dataset),
            pin_memory=False,
            drop_last=True,
            num_workers=config.num_workers,
            collate_fn=collate_fn,
    )
    val_loader = torch.utils.data.DataLoader(
            validation_dataset, 
            batch_size=config.batch_size,
            num_workers=config.num_workers,
            shuffle=False,
            sampler=SequentialSampler(validation_dataset),
            pin_memory=False,
            collate_fn=collate_fn,
    )

    print('train_loader_len:', len(train_loader))
    print('val_loader_len:', len(val_loader))

    base_config = get_efficientdet_config(config.model_name)
    base_config.image_size = config.im_size

    if(checkpoint_path):
        print(f'Resuming from checkpoint: {checkpoint_path}')        
        model = create_model_from_config(base_config, bench_task='train', bench_labeler=True,
                                 num_classes=config.num_classes,
                                 pretrained=False)
        model.to(device)
        
        fitter = Fitter(model=model, device=device, config=config, fold_num = val_fold)
        fitter.load(checkpoint_path)
    
    else:
        model = create_model_from_config(base_config, bench_task='train', bench_labeler=True,
                                     pretrained=True,
                                     num_classes=config.num_classes)
        model.to(device)
    
        fitter = Fitter(model=model, device=device, config=config, fold_num = val_fold)  
        
    model_config = model.config
    history_dict = fitter.fit(train_loader, val_loader)
    fold_history.append(history_dict)