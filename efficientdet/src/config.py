import torch
# from transformers import get_cosine_schedule_with_warmup
import os


# class Config:
#     num_workers = 4
#     batch_size = 10
#     n_epochs = 40 # n_epochs = 40
#     lr = 0.001
#     model_name = 'fasterrcnn_epochs=42_map=36822_augs=cutout(4)/randomcrop'
#     image_size = [512, 512]
#     folder = 'fasterrcnn-augmix_800_newone'
#     checkpoint_path = folder+'/last-checkpoint.bin'
#     test_weights_file = 'fasterrcnn-augmix_800_newone/last-checkpoint.bin'
#     # folder = 'effdet5'
#     train_images_path = '../input/vinbigdata/train/'
#     df_path = '../input/normalised_wbf_folds_t'
#     fold_number = 0

#     # -------------------
#     verbose = True
#     verbose_step = 1
#     # -------------------

#     # --------------------
#     step_scheduler = False  # do scheduler.step after optimizer.step
#     validation_scheduler = True  # do scheduler.step after validation stage loss

#     # SchedulerClass = torch.optim.lr_scheduler.OneCycleLR
#     # scheduler_params = dict(
#     #     max_lr=0.001,
#     #     epochs=n_epochs,
#     #     steps_per_epoch=int(2887 / batch_size),
#     #    pct_start=0.1,
#     #     final_div_factor=10**5
#     # )
    
#     SchedulerClass = torch.optim.lr_scheduler.ReduceLROnPlateau
#     scheduler_params = dict(
#          mode='max',
#          factor=0.8,
#          patience=2,
#          verbose=True, 
#          threshold=0.0001,
#          threshold_mode='abs',
#          cooldown=0, 
#          min_lr=1e-8,
#          eps=1e-08
#      )
#     # SchedulerClass = get_cosine_schedule_with_warmup
#     # scheduler_params = dict(
#     #     num_warmup_steps = 50,
#     #     num_training_steps = int((len(os.listdir('train_images'))-500)/batch_size)
#     # )


class Config:
    num_classes = 14
    num_workers = 2
    batch_size = 64
    n_epochs = 100
    lr = 0.0005
    seed = 42
    # model_name = 'tf_efficientdet_d1'
    model_name = 'resdet50'
    folder = '../models/training_job_3'
    train_images_path = '/content/drive/MyDrive/japan_detection/input/vinbigdata/train/'
    test_images_path = '/content/drive/MyDrive/japan_detection/input/vinbigdata/test/'
    df_path = '../input/normalised_wbf_folds_train.csv'
    # checkpoint_path = '/content/drive/MyDrive/japan_detection/models/training_job_2/last-checkpoint.bin'
    checkpoint_path = None
    im_size = (256, 256)
    fold_number = 0
    verbose = True
    verbose_step = 1
    step_scheduler = True
    validation_scheduler = False
    # n_img_count = len(df_annotations_wbf.image_id.unique())
    OptimizerClass = torch.optim.AdamW
    SchedulerClass = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts
    scheduler_params = dict(
                        T_0=50,
                        T_mult=1,
                        eta_min=0.0001,
                        last_epoch=-1,
                        verbose=False
                        )
    kfold = 5
    
    def reset(self):
        self.OptimizerClass = torch.optim.AdamW
        self.SchedulerClass = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts
