from torch.utils.data import Dataset
import pandas as pd
import torch
import os
import albumentations as A
import cv2
import numpy as np
from albumentations.augmentations.bbox_utils import convert_bboxes_to_albumentations
import time

class TrainDataset(Dataset):
    def __init__(self, image_path, marking, transforms=None, split='train', im_size=512):
        super().__init__()
        self.root = image_path
        self.marking = marking
        self.image_ids = marking['im_id'].unique()
        self.transforms = transforms
        self.split = split
        self.im_size = im_size

    def __getitem__(self, index: int):
        image_id = self.image_ids[index]
        # if self.split=='test' or random.random() > 0.5:
        #     image, boxes, labels = self.load_image_and_boxes(index)
        # else:
        #     image, boxes = self.load_cutmix_image_and_boxes(index)
        #     # to do: include labels here
        image, boxes, labels = self.load_image_and_boxes_and_labels(index)
        target = {}
        target['boxes'] = torch.tensor(boxes, dtype=torch.float)
        # print("Было:", target['boxes'])

        target['labels'] = torch.tensor(labels, dtype=torch.int64)
        target['image_id'] = torch.tensor(index)

        if self.transforms:
            sample = self.transforms(**{
                    'image': image,
                    'bboxes': target['boxes'],
                    'labels': labels
                })
            if len(sample['bboxes'])==0:
                while len(sample['bboxes'])==0:
                    sample = self.transforms(**{
                        'image': image,
                        'bboxes': target['boxes'],
                        'labels': labels
                })
            # t = time.time()
            image = sample['image']
            target['labels'] = torch.tensor(sample['labels'], dtype=torch.int64)
            target['boxes'] = torch.tensor(sample['bboxes'], dtype=torch.float)

        return image, target, index

    def __len__(self) -> int:
        return self.image_ids.shape[0]

    def load_image_and_boxes_and_labels(self, index):
        image_id = self.image_ids[index]
        curr_path = os.path.join(self.root, self.marking[self.marking['im_id']==image_id]['image_id'].values[0]+'.png')
        image = cv2.imread(curr_path, cv2.IMREAD_COLOR)
        # image = cv2.imread(self.marking[self.marking['im_id']==image_id]['image_path'].values[0], cv2.IMREAD_COLOR)
        # print(self.root, image_id)
        # print(self.marking[self.marking['im_id']==image_id]['image_path'].values[0])
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB).astype(np.float32)
        image /= 255.0
        records = self.marking[self.marking['im_id'] == image_id]
        labels = records['class_id'].values
        boxes = records[['x1', 'y1', 'x2', 'y2']].values*self.im_size
        return image, boxes, labels

    # def load_cutmix_image_and_boxes(self, index, imsize=1024):
    #     """ 
    #     This implementation of cutmix author:  https://www.kaggle.com/nvnnghia 
    #     Refactoring and adaptation: https://www.kaggle.com/shonenkov
    #     """
    #     w, h = imsize, imsize
    #     s = imsize // 2
    
    #     xc, yc = [int(random.uniform(imsize * 0.25, imsize * 0.75)) for _ in range(2)]  # center x, y
    #     indexes = [index] + [random.randint(0, self.image_ids.shape[0] - 1) for _ in range(3)]

    #     result_image = np.full((imsize, imsize, 3), 1, dtype=np.float32)
    #     result_boxes = []

    #     for i, index in enumerate(indexes):
    #         image, boxes = self.load_image_and_boxes(index)
    #         if i == 0:
    #             x1a, y1a, x2a, y2a = max(xc - w, 0), max(yc - h, 0), xc, yc  # xmin, ymin, xmax, ymax (large image)
    #             x1b, y1b, x2b, y2b = w - (x2a - x1a), h - (y2a - y1a), w, h  # xmin, ymin, xmax, ymax (small image)
    #         elif i == 1:  # top right
    #             x1a, y1a, x2a, y2a = xc, max(yc - h, 0), min(xc + w, s * 2), yc
    #             x1b, y1b, x2b, y2b = 0, h - (y2a - y1a), min(w, x2a - x1a), h
    #         elif i == 2:  # bottom left
    #             x1a, y1a, x2a, y2a = max(xc - w, 0), yc, xc, min(s * 2, yc + h)
    #             x1b, y1b, x2b, y2b = w - (x2a - x1a), 0, max(xc, w), min(y2a - y1a, h)
    #         elif i == 3:  # bottom right
    #             x1a, y1a, x2a, y2a = xc, yc, min(xc + w, s * 2), min(s * 2, yc + h)
    #             x1b, y1b, x2b, y2b = 0, 0, min(w, x2a - x1a), min(y2a - y1a, h)
    #         result_image[y1a:y2a, x1a:x2a] = image[y1b:y2b, x1b:x2b]
    #         padw = x1a - x1b
    #         padh = y1a - y1b

    #         boxes[:, 0] += padw
    #         boxes[:, 1] += padh
    #         boxes[:, 2] += padw
    #         boxes[:, 3] += padh

    #         result_boxes.append(boxes)

    #     result_boxes = np.concatenate(result_boxes, 0)
    #     np.clip(result_boxes[:, 0:], 0, 2 * s, out=result_boxes[:, 0:])
    #     result_boxes = result_boxes.astype(np.int32)
    #     result_boxes = result_boxes[np.where((result_boxes[:,2]-result_boxes[:,0])*(result_boxes[:,3]-result_boxes[:,1]) > 0)]
    #     return result_image, result_boxes




class TestDataset(Dataset):
    def __init__(self, path, transforms=None):
        super().__init__()
        self.path = path
        self.filenames = os.listdir(path)
        self.transforms = transforms

    def __getitem__(self, index: int):
        image_name = self.filenames[index]
        image = cv2.imread(os.path.join(self.path, image_name), cv2.IMREAD_COLOR)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB).astype(np.float32)
        image /= 255.0
        if self.transforms:
            sample = {'image': image}
            sample = self.transforms(**sample)
            image = sample['image']
        return image, image_name

    def __len__(self) -> int:
        return len(self.filenames)




