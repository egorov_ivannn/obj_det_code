import torchvision
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor
from config import Config as config
# from tqdm import tqdm
import os
import torch
from transforms import get_test_transform
from dataset import TestDataset
from torch.utils.data import DataLoader
import numpy as np
from effdet import get_efficientdet_config, EfficientDet, DetBenchTrain
from effdet import create_model, unwrap_bench, create_loader, create_dataset, create_evaluator, create_model_from_config
from tqdm import tqdm
from ensemble_boxes import *
import pandas as pd

dataset = TestDataset(
    path=config.test_images_path,
    transforms=get_test_transform()
)

def collate_fn(batch):
    return tuple(zip(*batch))

batch_size = 1
data_loader = DataLoader(
    dataset,
    batch_size=batch_size,
    shuffle=False,
    num_workers=4,
    drop_last=False,
    collate_fn=collate_fn
)

idx2label = {
            0: "Aortic enlargement",
            1: "Atelectasis",
            2: "Calcification",
            3: "Cardiomegaly",
            4: "Consolidation",
            5: "ILD",
            6: "Infiltration",
            7: "Lung Opacity",
            8: "Nodule/Mass",
            9: "Other lesion",
            10: "Pleural effusion",
            11: "Pleural thickening",
            12: "Pneumothorax",
            13: "Pulmonary fibrosis"
            }


# # load a model; pre-trained on COCO
# model = torchvision.models.detection.fasterrcnn_resnet50_fpn(
#     pretrained=False,
#     pretrained_backbone=False,
#     min_size=512,
#     max_size=853
# )

base_config = get_efficientdet_config(config.model_name)
base_config.image_size = config.im_size

device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
WEIGHTS_FILE = '/content/drive/MyDrive/japan_detection/models/training_job_2/fold_3_best-checkpoint-072epoch_loss=1.564.bin'
checkpoint = torch.load(WEIGHTS_FILE)

model = create_model_from_config(base_config, bench_task='predict', bench_labeler=True,
                                 pretrained=False,
                                 num_classes=config.num_classes)
model.model.load_state_dict(checkpoint['model_state_dict'])
model.to(device)

def format_prediction_string(labels, boxes, scores):
    pred_strings = []
    for j in zip(labels, scores, boxes):
        pred_strings.append("{0} {1:.4f} {2} {3} {4} {5}".format(
            j[0], j[1], j[2][0], j[2][1], j[2][2], j[2][3]))

    return " ".join(pred_strings)

detection_threshold = 0


results = pd.DataFrame(columns = ['image_id', 'PredictionString'])

model.eval()

test_df = pd.read_csv('/content/drive/MyDrive/japan_detection/input/vinbigdata/test.csv')

with torch.no_grad():
    for images, image_ids in tqdm(data_loader):
        images = torch.stack(images)
        images = images.to(device).float()
        outputs = model(images)
        for i, image in enumerate(images):
            image_id = image_ids[i][:-4]
            width, height = test_df[test_df['image_id']==image_id].iloc[0, 1], test_df[test_df['image_id']==image_id].iloc[0, 2]
            result = {
                'image_id': image_id,
                'PredictionString': '14 1.0 0 0 1 1'
            }
            
            boxes = outputs[i][:, :4].detach().cpu().numpy()
            boxes[:, 0] *= (1/config.im_size[0])
            boxes[:, 2] *= (1/config.im_size[0])
            boxes[:, 1] *= (1/config.im_size[0])
            boxes[:, 3] *= (1/config.im_size[0])
            labels = outputs[i][:, 5].detach().cpu().numpy()
            scores = outputs[i][:, 4].detach().cpu().numpy()

            boxes, scores, labels = weighted_boxes_fusion([boxes.tolist()], [scores.tolist()], [labels.tolist()], weights=None, iou_thr=0.6)

            boxes[:, 0] *= width
            boxes[:, 2] *= width
            boxes[:, 1] *= height
            boxes[:, 3] *= height

            if len(boxes) > 0:

                selected = scores >= detection_threshold
                boxes = boxes[selected].astype(np.int32)
                scores = scores[selected]
                labels = labels[selected].astype(np.int32)

                if len(boxes) > 0:
                    result = {
                        'image_id': image_id,
                        'PredictionString': format_prediction_string(labels, boxes, scores)
                    }
            results.loc[len(results)] = [result['image_id'], result['PredictionString']]

results.to_csv('sumbission.csv', index=False)
# res = {}
# for k in results:
#     res[k['image_id']] = {idx2label[1]:[], idx2label[5]:[]}
#     arr = np.array([float(i) for i in k['PredictionString'].split(' ')]).reshape(-1, 6)
#     idxs = np.argsort(arr[:, 1].astype(float))
#     for j in arr[idxs]:
#         if int(j[0])==1 or int(j[0])==5:
#             res[k['image_id']][idx2label[int(j[0])]].append(j[2:].tolist())

# import json
# with open(os.path.join(Config.folder, 'submit.json'), 'w') as fp:
#     json.dump(res, fp, sort_keys=True, indent=4)