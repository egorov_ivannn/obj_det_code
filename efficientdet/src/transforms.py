from albumentations.pytorch.transforms import ToTensorV2
import albumentations as A
from config import Config


# Albumentations
def get_train_transforms():
    return A.Compose([
        A.HorizontalFlip(p=0.5),
        A.VerticalFlip(p=0.5),
        A.ShiftScaleRotate(scale_limit=0.1, rotate_limit=45, p=0.25),
        A.OneOf([
                A.Cutout(num_holes=8, max_h_size=8, max_w_size=8,\
                         fill_value=0, p=0.5),
                A.RandomSizedCrop(min_max_height=(64, 256),\
                                 height=256, width=256, p=0.5), 
                # A.RandomGridShuffle(grid=(2, 2), p=0.5) 
        ], p=0.8),
        # A.LongestMaxSize(max_size=800, p=1.0),
        A.ColorJitter(p=0.5),
        A.GaussNoise(p=0.5),
        # A.Normalize(mean=(0, 0, 0), std=(1, 1, 1), max_pixel_value=255.0, p=1.0),
        ToTensorV2(p=1.0)
    ], bbox_params={'format': 'pascal_voc', 'label_fields': ['labels']})

def get_valid_transforms():
    return A.Compose([
        # A.Normalize(mean=(0, 0, 0), std=(1, 1, 1), max_pixel_value=255.0, p=1.0),
        ToTensorV2(p=1.0)
    ], bbox_params={'format': 'pascal_voc', 'label_fields': ['labels']})

def get_test_transform():
    return A.Compose([
        # A.Resize(800, 800),
        # A.Normalize(mean=(0, 0, 0), std=(1, 1, 1), max_pixel_value=255.0, p=1.0),
        ToTensorV2(p=1.0)
    ])

# def get_valid_transforms():
#     return A.Compose(
#         [   A.Normalize(always_apply=True),
#             A.Resize(height=Config.image_size[0],\
#                      width=Config.image_size[1], p=1.0),
#             ToTensorV2(p=1.0),
#         ], 
#         p=1.0, 
#         bbox_params=A.BboxParams(
#             format='pascal_voc',
#             min_area=0, 
#             min_visibility=0,
#             label_fields=['labels']
#         )
#     )

# def get_train_transforms():
#     return A.Compose(
#         [   
#             A.Normalize(always_apply=True),
#             # A.RandomSizedCrop(min_max_height=(256, 2160),\
#             #                      height=2160, width=3840, p=1),
#             A.OneOf([
#                 A.HueSaturationValue(hue_shift_limit=0.2, sat_shift_limit= 0.2, 
#                                      val_shift_limit=0.2, p=0.9),
#                 A.RandomBrightnessContrast(brightness_limit=0.2, 
#                                            contrast_limit=0.2, p=0.9),
#             ],p=0.9),
#             A.ColorJitter(p=0.7),
#             A.GaussNoise(),
#             A.ToGray(p=0.01),
#             A.HorizontalFlip(p=0.5),
#             A.VerticalFlip(p=0.5),
#             A.Resize(height=Config.image_size[0], width=Config.image_size[1], p=1),
#             ToTensorV2(p=1.0),

#         ], 
#         p=1.0, 
#         bbox_params=A.BboxParams(
#             format='pascal_voc',
#             min_area=0, 
#             min_visibility=0,
#             label_fields=['labels']
#         )
#     )


def get_reverse_transforms():
    return A.Compose(
        [
            A.Resize(height=3840, width=2160, p=1.0),
            # ToTensorV2(p=1.0)
        ], 
        p=1.0, 
        bbox_params=A.BboxParams(
            format='pascal_voc',
            min_area=0, 
            min_visibility=0,
            label_fields=['labels']
        )
    )