import torch
import os
import warnings
warnings.filterwarnings("ignore")
from datetime import datetime
import time
import numpy as np
from glob import glob
from metrics import calculate_image_precision
import numba
from tqdm.contrib.telegram import tqdm, trange
from tqdm import tqdm as tqdm_simple
import copy
import matplotlib.pyplot as plt
from effdet import get_efficientdet_config, EfficientDet, DetBenchTrain


class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count




class Fitter:
    def __init__(self, model, device, config, fold_num):
        self.config = config
        self.epoch = 0
        self.fold_num = fold_num
        self.base_dir = f'./{config.folder}'
        
        if not os.path.exists(self.base_dir):
            os.makedirs(self.base_dir)
        
        self.log_path = f'{self.base_dir}/log.txt'
        self.best_summary_loss = 10**5
        self.model = model
        self.device = device

        param_optimizer = list(self.model.named_parameters())
        no_decay = ['bias', 'LayerNorm.bias', 'LayerNorm.weight']
        optimizer_grouped_parameters = [
            {'params': [p for n, p in param_optimizer if not any(nd in n for nd in no_decay)], 'weight_decay': 0.001},
            {'params': [p for n, p in param_optimizer if any(nd in n for nd in no_decay)], 'weight_decay': 0.0}
        ] 

        self.optimizer = config.OptimizerClass(self.model.parameters(), lr=config.lr)
        self.scheduler = config.SchedulerClass(self.optimizer, **config.scheduler_params)
        self.log(f'Fitter prepared. Device is {self.device}')

    def fit(self, train_loader, validation_loader):
        history_dict = {}
        history_dict['epoch'] = []
        history_dict['train_loss'] = []
        history_dict['val_loss'] = []
        history_dict['train_lr'] = []
        # train_loss, val_loss = 0, 0
        tr = trange(self.config.n_epochs, token='1698282106:AAHjkxwz7vE8FY02pbW-vfkjWbCHCGJYFZA', chat_id='556151723')
        for e in tr:
            history_dict['epoch'].append(self.epoch)
            lr = self.optimizer.param_groups[0]['lr']
            timestamp = datetime.utcnow().isoformat()
            
            if self.config.verbose:
                self.log(f'\n{timestamp}\nLR: {lr}')

            t = time.time()
            train_loss, loss_trend, lr_trend = self.train_epoch(train_loader)
            
            history_dict['train_loss'].append(loss_trend)
            history_dict['train_lr'].append(lr_trend)
            self.log(f'[RESULT]: Train. Epoch: {self.epoch}, summary_loss: {train_loss.avg:.5f}, time: {(time.time() - t):.5f}')
            self.save(f'{self.base_dir}/last-checkpoint.bin')
            
            t = time.time()
            val_loss, loss_trend = self.validation(validation_loader)
            history_dict['val_loss'].append(loss_trend)
            self.log(f'[RESULT]: Val. Epoch: {self.epoch}, summary_loss: {val_loss.avg:.5f}, time: {(time.time() - t):.5f}')
            
            if val_loss.avg < self.best_summary_loss:
                self.best_summary_loss = val_loss.avg
                self.model.eval()
                checkpoint_name = f'{self.base_dir}/fold_{self.fold_num}_best-checkpoint-{str(self.epoch).zfill(3)}epoch_loss={val_loss.avg:.3f}.bin'
                self.save(checkpoint_name)
                
                try:
                    os.remove(f)
                except:pass
                f = copy.deepcopy(checkpoint_name)

            if self.config.validation_scheduler:
                self.scheduler.step(metrics=val_loss.avg)

            self.epoch += 1

            tr.set_description(f'Fold: {self.fold_num}, train_loss: {train_loss.avg:.5f}, val_loss: {val_loss.avg:.5f}, best: {self.best_summary_loss:.5f}')
            tr.refresh() # to show immediately the update

        
        plt.figure(figsize=(15, 10))
        plt.plot([np.mean(i) for i in history_dict['train_loss']], label = 'train_loss')
        plt.plot([np.mean(i) for i in history_dict['val_loss']], label = 'val_loss')
        plt.legend()
        plt.savefig(os.path.split(self.log_path)[0]+f'/metrics_fold{self.fold_num}.jpg')

        return history_dict

    def train_epoch(self, train_loader):
        self.model.train()
        summary_loss = AverageMeter()
        t = time.time()
        loss_trend = []
        lr_trend = []
        # for step, (images, targets, image_ids) in tqdm_simple(enumerate(train_loader), total=len(train_loader)):
        for step, (images, targets, image_ids) in enumerate(train_loader):
            if self.config.verbose:
                if step % self.config.verbose_step == 0:
                    print(
                        f'Train Step {step}/{len(train_loader)}, ' + \
                        f'summary_loss: {summary_loss.avg:.5f}, ' + \
                        f'time: {(time.time() - t):.5f}', end='\r'
                    )            
            
            images = torch.stack(images)
            images = images.to(self.device).float()
            target_res = {}
            boxes = [target['boxes'].to(self.device).float() for target in targets]
            print(boxes)
            labels = [target['labels'].to(self.device).float() for target in targets]
            target_res['bbox'] = boxes
            target_res['cls'] = labels
            self.optimizer.zero_grad()
            output = self.model(images, target_res)
            loss = output['loss']
            loss.backward()
            summary_loss.update(loss.detach().item(), self.config.batch_size)
            self.optimizer.step()

            if self.config.step_scheduler:
                self.scheduler.step()

            
            lr = self.optimizer.param_groups[0]['lr']
            loss_trend.append(summary_loss.avg)
            lr_trend.append(lr)
        return summary_loss, loss_trend, lr_trend
    
    def validation(self, val_loader):
        self.model.eval()
        summary_loss = AverageMeter()
        t = time.time()
        loss_trend = []
#         lr_trend = []
        
        # for step, (images, targets, image_ids) in tqdm_simple(enumerate(val_loader), total=len(val_loader)):
        for step, (images, targets, image_ids) in enumerate(val_loader):
            if self.config.verbose:
                if step % self.config.verbose_step == 0:
                    print(
                        f'Val Step {step}/{len(val_loader)}, ' + \
                        f'summary_loss: {summary_loss.avg:.5f}, ' + \
                        f'time: {(time.time() - t):.5f}', end='\r'
                    )
            with torch.no_grad():
                images = torch.stack(images)
                images = images.to(self.device).float()
                target_res = {}
                boxes = [target['boxes'].to(self.device).float() for target in targets]
                labels = [target['labels'].to(self.device).float() for target in targets]
                target_res['bbox'] = boxes
                target_res['cls'] = labels 
                target_res["img_scale"] = torch.tensor([1.0] * self.config.batch_size,
                                                       dtype=torch.float).to(self.device)
                target_res["img_size"] = torch.tensor([images[0].shape[-2:]] * self.config.batch_size,
                                                      dtype=torch.float).to(self.device)
                
                output = self.model(images, target_res)
            
                loss = output['loss']
                summary_loss.update(loss.detach().item(), self.config.batch_size)

                loss_trend.append(summary_loss.avg)
        return summary_loss, loss_trend
    
    
    def save(self, path):
        self.model.eval()
        torch.save({
            'model_state_dict': self.model.model.state_dict(),
            'optimizer_state_dict': self.optimizer.state_dict(),
            'scheduler_state_dict': self.scheduler.state_dict(),
            'best_summary_loss': self.best_summary_loss,
            'epoch': self.epoch,
        }, path)

    def load(self, path):
        checkpoint = torch.load(path)
        self.model.model.load_state_dict(checkpoint['model_state_dict'])
        self.optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        self.scheduler.load_state_dict(checkpoint['scheduler_state_dict'])
        self.best_summary_loss = checkpoint['best_summary_loss']
        self.epoch = checkpoint['epoch'] + 1
        
    def log(self, message):
        if self.config.verbose:
            print(message)
        with open(self.log_path, 'a+') as logger:
            logger.write(f'{message}\n')