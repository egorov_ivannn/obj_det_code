import albumentations as A
import copy
import logging
import torch
import numpy as np

from detectron2.data import detection_utils as utils
import detectron2.data.transforms as T



def get_augs(split: str = 'train'):
    img_norm_cfg = dict(mean=[172.74739404, 126.57150786, 101.4188272 ], std=[38.04224913, 44.24741661, 43.2202725 ], to_rgb=True)
    
    if split=='train':
        # augs_dict = {}
        augs_dict = [
                dict(type="RandomRotate90", p=0.5),
                dict(
                    type="OneOf",
                    transforms=[
                        dict(type="HueSaturationValue", hue_shift_limit=10, sat_shift_limit=35, val_shift_limit=25),
                        dict(type="RandomGamma"),
                        dict(type="CLAHE"),
                    ],
                    p=0.5,
                ),
                dict(
                    type="OneOf",
                    transforms=[
                        dict(type="RandomBrightnessContrast", brightness_limit=0.25, contrast_limit=0.25),
                        dict(type="RGBShift", r_shift_limit=15, g_shift_limit=15, b_shift_limit=15),
                    ],
                    p=0.5,
                ),
                dict(
                    type="OneOf",
                    transforms=[
                        dict(type="Blur"),
                        dict(type="MotionBlur"),
                        dict(type="GaussNoise"),
                        dict(type="ImageCompression", quality_lower=75),
                    ],
                    p=0.4,
                ),


                dict(
                    type="Cutout",
                    num_holes=8,
                    max_h_size=16,
                    max_w_size=16,
                    fill_value=img_norm_cfg["mean"][::-1],
                    p=0.4,
                ),
                dict(
                    type="ShiftScaleRotate",
                    shift_limit=0.3,
                    rotate_limit=5,
                    scale_limit=(-0.3, 0.75),
                    border_mode=0,
                    value=img_norm_cfg["mean"][::-1],
                ),
                # dict(type="RandomBBoxesSafeCrop", num_rate=(0.5, 1.0), erosion_rate=0.2),
            ]

    elif split=='test' or split=='valid':
        augs_dict = {}


    else:
        raise ValueError(f"[ERROR] Unexpected value split={split}")
    return augs_dict





class MyMapper:
    """Mapper which uses `detectron2.data.transforms` augmentations"""

    def __init__(self, cfg, is_train: bool = True):
        aug_kwargs = cfg.aug_kwargs
        aug_list = [
            # T.Resize((800, 800)),
        ]
        if is_train:
            aug_list.extend([getattr(T, name)(**kwargs) for name, kwargs in aug.items() for aug in aug_kwargs])
        self.augmentations = T.AugmentationList(aug_list)
        self.is_train = is_train

        mode = "training" if is_train else "inference"
        print(f"[MyDatasetMapper] Augmentations used in {mode}: {self.augmentations}")

    def __call__(self, dataset_dict):
        dataset_dict = copy.deepcopy(dataset_dict)  # it will be modified by code below
        image = utils.read_image(dataset_dict["file_name"], format="BGR")

        aug_input = T.AugInput(image)
        transforms = self.augmentations(aug_input)
        image = aug_input.image

        # if not self.is_train:
        #     # USER: Modify this if you want to keep them for some reason.
        #     dataset_dict.pop("annotations", None)
        #     dataset_dict.pop("sem_seg_file_name", None)
        #     return dataset_dict

        image_shape = image.shape[:2]  # h, w
        dataset_dict["image"] = torch.as_tensor(image.transpose(2, 0, 1).astype("float32"))
        annos = [
            utils.transform_instance_annotations(obj, transforms, image_shape)
            for obj in dataset_dict.pop("annotations")
            if obj.get("iscrowd", 0) == 0
        ]
        instances = utils.annotations_to_instances(annos, image_shape)
        dataset_dict["instances"] = utils.filter_empty_instances(instances)
        return dataset_dict





class AlbumentationsMapper:
    """Mapper which uses `albumentations` augmentations"""
    def __init__(self, cfg, is_train: bool = True):
        aug_kwargs = cfg.aug_kwargs
        aug_list = []

        if is_train:
            # aug_list.extend([getattr(A, name)(**kwargs) for name, kwargs in aug_kwargs.items()])
            aug_list = get_augs_from_list_of_dicts(aug_kwargs)
        self.transform = A.Compose(
            aug_list, bbox_params=A.BboxParams(format="pascal_voc", label_fields=["category_ids"])
        )
        self.is_train = is_train

        mode = "training" if is_train else "inference"
        print(f"[AlbumentationsMapper] Augmentations used in {mode}: {self.transform}")

    def __call__(self, dataset_dict):
        dataset_dict = copy.deepcopy(dataset_dict)  # it will be modified by code below
        image = utils.read_image(dataset_dict["file_name"], format="BGR")

        # aug_input = T.AugInput(image)
        # transforms = self.augmentations(aug_input)
        # image = aug_input.image

        prev_anno = dataset_dict["annotations"]
        bboxes = np.array([obj["bbox"] for obj in prev_anno], dtype=np.float32)
        # category_id = np.array([obj["category_id"] for obj in dataset_dict["annotations"]], dtype=np.int64)
        category_id = np.arange(len(dataset_dict["annotations"]))

        transformed = self.transform(image=image, bboxes=bboxes, category_ids=category_id)
        image = transformed["image"]
        annos = []
        for i, j in enumerate(transformed["category_ids"]):
            d = prev_anno[j]
            d["bbox"] = transformed["bboxes"][i]
            annos.append(d)
        dataset_dict.pop("annotations", None)  # Remove unnecessary field.

        # if not self.is_train:
        #     # USER: Modify this if you want to keep them for some reason.
        #     dataset_dict.pop("annotations", None)
        #     dataset_dict.pop("sem_seg_file_name", None)
        #     return dataset_dict

        image_shape = image.shape[:2]  # h, w
        dataset_dict["image"] = torch.as_tensor(image.transpose(2, 0, 1).astype("float32"))
        instances = utils.annotations_to_instances(annos, image_shape)
        dataset_dict["instances"] = utils.filter_empty_instances(instances)
        return dataset_dict


def get_augs_from_list_of_dicts(augs_dict):
    aug_list = []
    for aug in augs_dict:
        if aug['type'] == 'OneOf':
            oneof = []
            for i in aug['transforms']:
                kwargs = {name:kwarg for name, kwarg in i.items() if name!='type'}
                oneof.append(getattr(A, i['type'])(**kwargs))
            aug_list.append(A.OneOf(oneof, p=aug['p']))
        else:
            kwargs = {name:kwarg for name, kwarg in aug.items() if name!='type'}
            aug_list.append(getattr(A, aug['type'])(**kwargs))
    return aug_list