import os
from dataclasses import dataclass, field
from typing import Dict
from pathlib import Path
from typing import Any, Union

import yaml


@dataclass
class Flags:
    # General
    debug: bool = True
    outdir: str = "results/det"

    data_dir: str = '/content/drive/MyDrive/detectron_models'
    outdir = os.path.join(data_dir, 'experiments', "exp_" + str(len(os.listdir('/content/drive/MyDrive/detectron_models/experiments'))))

    # Data config
    df_path = os.path.join(data_dir, 'bboxes.csv')
    imgdir: str = "/content/drive/japan_detection/train_512"
    split_mode: str = "all_train"  # all_train or valid20
    
    seed: int = 111
    im_size: int = 512

    # Training config
    iter: int = 10000
    batch_size: int = 16  # images per batch, this corresponds to "total batch size"
    num_workers: int = 4

    lr_scheduler_name: str = "WarmupMultiStepLR"  # WarmupMultiStepLR (default) or WarmupCosineLR]
    model_name: str = 'Misc/cascade_mask_rcnn_X_152_32x8d_FPN_IN5k_gn_dconv.yaml'

    base_lr: float = 0.00025
    roi_batch_size_per_image: int = 1024
    eval_period: int = 10000
    save_every: int = 10000
    aug_kwargs: Dict = field(default_factory=lambda: {})
    # aug_kwargs: Dict = {}

    def update(self, param_dict: Dict) -> "Flags":
        # Overwrite by `param_dict`
        for key, value in param_dict.items():
            if not hasattr(self, key):
                raise ValueError(f"[ERROR] Unexpected key for flag = {key}")
            setattr(self, key, value)
        return self




def save_yaml(filepath: Union[str, Path], content: Any, width: int = 120):
    with open(filepath, "w") as f:
        yaml.dump(content, f, width=width)


def load_yaml(filepath: Union[str, Path]) -> Any:
    with open(filepath, "r") as f:
        content = yaml.full_load(f)
    return content


