import time
import numpy as np
import torch
from config import save_yaml, load_yaml
import pandas as pd
from class_fixing import get_labels_stuff
from detectron2.config import get_cfg
from detectron2.engine import DefaultPredictor, DefaultTrainer, launch
from detectron2.data import DatasetCatalog, MetadataCatalog
from detectron2 import model_zoo
import os
from dataset import get_data_dicts
from tqdm import tqdm
import json
import cv2
from ensemble_boxes import *

def soft_nms_pytorch(dets, box_scores, sigma=0.5, thresh=0.001, cuda=0):
    """
    Build a pytorch implement of Soft NMS algorithm.
    # Augments
        dets:        boxes coordinate tensor (format:[y1, x1, y2, x2])
        box_scores:  box score tensors
        sigma:       variance of Gaussian function
        thresh:      score thresh
        cuda:        CUDA flag
    # Return
        the index of the selected boxes
    """

    # Indexes concatenate boxes with the last column
    N = dets.shape[0]
    if cuda:
        indexes = torch.arange(0, N, dtype=torch.float).cuda().view(N, 1)
    else:
        indexes = torch.arange(0, N, dtype=torch.float).view(N, 1)
    dets = torch.cat((dets, indexes), dim=1)

    # The order of boxes coordinate is [y1,x1,y2,x2]
    y1 = dets[:, 0]
    x1 = dets[:, 1]
    y2 = dets[:, 2]
    x2 = dets[:, 3]
    scores = box_scores
    areas = (x2 - x1 + 1) * (y2 - y1 + 1)

    for i in range(N):
        # intermediate parameters for later parameters exchange
        tscore = scores[i].clone()
        pos = i + 1

        if i != N - 1:
            maxscore, maxpos = torch.max(scores[pos:], dim=0)
            if tscore < maxscore:
                dets[i], dets[maxpos.item() + i + 1] = dets[maxpos.item() + i + 1].clone(), dets[i].clone()
                scores[i], scores[maxpos.item() + i + 1] = scores[maxpos.item() + i + 1].clone(), scores[i].clone()
                areas[i], areas[maxpos + i + 1] = areas[maxpos + i + 1].clone(), areas[i].clone()

        # IoU calculate
        yy1 = np.maximum(dets[i, 0].to("cpu").numpy(), dets[pos:, 0].to("cpu").numpy())
        xx1 = np.maximum(dets[i, 1].to("cpu").numpy(), dets[pos:, 1].to("cpu").numpy())
        yy2 = np.minimum(dets[i, 2].to("cpu").numpy(), dets[pos:, 2].to("cpu").numpy())
        xx2 = np.minimum(dets[i, 3].to("cpu").numpy(), dets[pos:, 3].to("cpu").numpy())
        
        w = np.maximum(0.0, xx2 - xx1 + 1)
        h = np.maximum(0.0, yy2 - yy1 + 1)
        inter = torch.tensor(w * h).cuda() if cuda else torch.tensor(w * h)
        ovr = torch.div(inter, (areas[i] + areas[pos:] - inter))

        # Gaussian decay
        weight = torch.exp(-(ovr * ovr) / sigma)
        scores[pos:] = weight * scores[pos:]

    # select the boxes and keep the corresponding indexes
    keep = dets[:, 4][scores > thresh].int()

    return keep

# flags = load_yaml('/content/drive/MyDrive/detectron_models/experiments/exp_1/flags.yaml')
# print(flags)
df = pd.read_csv('/content/drive/MyDrive/detectron_models/bboxes.csv')
labels_stuff  = get_labels_stuff(df)
labels_list = labels_stuff['labels_list']

DatasetCatalog.register(
    "fish_test", lambda: get_data_dicts(df=None, imdir = os.path.abspath('/content/drive/MyDrive/japan_detection/test_1024'), split='test')
)
MetadataCatalog.get("fish_test").set(thing_classes=labels_list)
# metadata = MetadataCatalog.get("fish_test")


cfg = get_cfg()
# cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url("COCO-Detection/retinanet_R_101_FPN_3x.yaml")
cfg.merge_from_file(model_zoo.get_config_file("COCO-Detection/faster_rcnn_R_101_DC5_3x.yaml"))
cfg.MODEL.WEIGHTS = os.path.join('/content/drive/MyDrive/detectron_models/experiments/exp_16/', "model_0059999.pth")
# cfg.MODEL.ROI_HEADS.BATCH_SIZE_PER_IMAGE = 512
cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.0
cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(labels_list)
cfg.DATASETS.TEST = ("fish_test",)
cfg.MIN_SIZE_TEST = 512
cfg.OUTPUT_DIR = os.path.abspath('/content/drive/MyDrive/detectron_models/experiments/exp_16/')
# print(cfg)
predictor = DefaultPredictor(cfg)

res = {}
dataset_dicts = get_data_dicts(df=None, imdir = os.path.abspath('/content/drive/MyDrive/japan_detection/test_1024'), split='test')
for idx, i in tqdm(enumerate(dataset_dicts)):
    im_name  = i['image_id']+'.jpg'
    res[im_name] = {labels_list[0]:[],labels_list[2]:[]}
    im = cv2.imread(i["file_name"], cv2.IMREAD_COLOR)
    output = predictor(im)

    b = output['instances'].pred_boxes.tensor.cpu().numpy()
    # print(b)
    s = output['instances'].scores.cpu().numpy()
    c = output['instances'].pred_classes.cpu().numpy()
    # print(c)
    # print(len(c))
    b = b[(c==0) | (c==2)]
    s = s[(c==0) | (c==2)]
    c = c[(c==0) | (c==2)]

    # b, s, l = weighted_boxes_fusion([(b/1024).tolist()], [s.tolist()], [c.tolist()], weights=None, iou_thr=0.6)

    # idxs = nms.nms.boxes(b, s)
    if len(c)==0:
        continue
    # idxs = soft_nms_pytorch(torch.tensor(b), torch.tensor(s), thresh=0).tolist()
    # for i in idxs:
    for j in range(len(b)):
        # print(output['instances'][0])
        # item = output['instances'][[i]]
        item = b[j]
        # print(item)
        label = c[j]
        # print(label)
        # boxes = item.pred_boxes.tensor.cpu().numpy()[0]/512
        # boxes = item
        boxes = item/1024
        boxes[0] *= 3840
        boxes[2] *= 3840
        boxes[1] *= 2160
        boxes[3] *= 2160
        # print(boxes)
        curr_class = res[im_name][labels_list[label]]
        # curr_class = res[im_name][labels_list[item.pred_classes.item()]]
        if len(curr_class)<10:
            curr_class.append(boxes.tolist())



with open('submit.json', 'w') as fp:
    json.dump(res, fp, sort_keys=True, indent=4)