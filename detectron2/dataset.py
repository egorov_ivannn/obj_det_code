import os
import pandas as pd
from detectron2.structures import BoxMode
from tqdm import tqdm
import cv2
from config import Flags as config

def get_data_dicts(
                df: pd.DataFrame or None,
                imdir: os.path,
                split: str = 'train',
                debug: bool = False
            ):
    if split == 'train' and type(df)!=type(None):
        data_dicts = []
        temp_im = cv2.imread(os.path.join(imdir, df.loc[0, 'image_name']+'.jpg'))
        height, width = temp_im.shape[:2]

        if debug:
            df = df[:50]


        for item in tqdm(df['image_name'].unique()):
            temp_df = df[df['image_name']==item].copy()

            data_dicts.append({
                'annotations': [{"bbox": list(box),
                                "bbox_mode": BoxMode.XYXY_ABS,
                                "category_id": label}\
                                for box, label in zip(temp_df[['x1', 'y1', 'x2', 'y2']].values,\
                                                      temp_df['encoded_labels'].values)],
                "file_name": os.path.join(imdir, item+'.jpg'),
                "image_id": item,
                "height": height,
                "width": width
                
            })
        return data_dicts

    elif split == 'test' and df==None:
        data_dicts = []
        for idx, filename in tqdm(enumerate(os.listdir(imdir))):
            if idx==0:
                temp_im = cv2.imread(os.path.join(imdir, filename))
                height, width = temp_im.shape[:2]
            data_dicts.append({
                    "file_name": os.path.join(imdir, filename),
                    "image_id": filename[:-4],
                    "height": height,
                    "width": width
                })
            
        return data_dicts

