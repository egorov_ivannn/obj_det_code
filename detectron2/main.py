from config import Flags as config
from dataclasses import dataclass, field
import copy 
import cv2
from class_fixing import get_labels_stuff
import os
import dataclasses
from config import save_yaml, load_yaml
import pandas as pd
from dataset import get_data_dicts
from detectron2.data import DatasetCatalog, MetadataCatalog
import numpy as np
from detectron2.config.config import CfgNode as CN
from train import run_train
from transforms import get_augs

flags_dict = {
    # "debug": False,
    
    "imgdir": os.path.abspath('/content/drive/MyDrive/japan_detection/train_1024'),
    "split_mode": "all_train",
    "iter": 60000,
    "roi_batch_size_per_image": 1024,
    # "eval_period": 5000,
    "lr_scheduler_name": "WarmupCosineLR",
    "base_lr": 0.005,
    "num_workers": 4,
    "batch_size": 5,
    "im_size": 1024,
    "model_name": 'COCO-Detection/faster_rcnn_R_101_DC5_3x.yaml',
    'aug_kwargs': get_augs(split='train'), 
    'save_every': 10000,
    'debug': False, 
    # 'outdir': os.path.join('/content/drive/MyDrive/detectron_models', 'experiments', "exp_14")

}

# args = parse()
flags = config().update(flags_dict)
outdir = flags.outdir
os.makedirs(str(outdir), exist_ok=True)
flags_dict = dataclasses.asdict(flags)
save_yaml(os.path.join(outdir, "flags.yaml"), flags)


# # Read in the data CSV files
df = pd.read_csv(config.df_path)

labels_stuff = get_labels_stuff(df)
labels_list = labels_stuff['labels_list']


split_mode = flags.split_mode

if split_mode == "all_train":
    df.loc[:, df.columns[4:12]] = df.loc[:, df.columns[4:12]]*flags.im_size
    DatasetCatalog.register(
        "fish_train",
        lambda: get_data_dicts(df, flags.imgdir, debug = flags.debug),
    )
    MetadataCatalog.get("fish_train").set(thing_classes=labels_list)

elif split_mode == "valid20":
    n_train = int(len(df['image_name'].unique()) * 0.8)
    idxs = np.random.RandomState(flags.seed).choice(df['image_name'].unique(), size=n_train, replace=False)

    df.index = df.image_name.values
    train_df = copy.deepcopy(df.loc[idxs, :])
    train_df.index=range(len(train_df))
    train_df.loc[:, train_df.columns[4:12]] = train_df.loc[:, train_df.columns[4:12]]*flags.im_size

    DatasetCatalog.register(
        "fish_train",
        lambda: get_data_dicts(train_df, flags.imgdir, debug = flags.debug),
    )
    MetadataCatalog.get("fish_train").set(thing_classes=labels_list)


    valid_df = copy.deepcopy(df.drop(index=idxs))
    valid_df.index=range(len(valid_df))
    valid_df.loc[:, valid_df.columns[4:12]] = valid_df.loc[:, valid_df.columns[4:12]]*flags.im_size

    DatasetCatalog.register(
        "fish_valid",
        lambda: get_data_dicts(valid_df, flags.imgdir, debug = flags.debug),
    )
    MetadataCatalog.get("fish_valid").set(thing_classes=labels_list)

else:
    raise ValueError(f"[ERROR] Unexpected value split_mode={split_mode}")

if __name__ == "__main__":
    run_train(flags, labels_list)
