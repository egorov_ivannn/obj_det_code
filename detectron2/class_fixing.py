from config import Flags as config

import pandas as pd
from config import Flags
from sklearn.preprocessing import LabelEncoder
import os


# df = pd.read_csv(Flags.df_path)

# bad_classes = ['Bird', 'Dolphin', 'Each Fish', 'Object', 'w']

# for i in bad_classes:
#     df = df[df['label']!=i]

# encoder = LabelEncoder()
# df['encoded_labels'] = encoder.fit_transform(df.label)
# df.to_csv('bboxes.csv', index=False)

def get_labels_stuff(df):
    idx2label = {}
    for i in df.itertuples():
        temp = df.sort_values(by='encoded_labels').copy()
        if i.label not in idx2label.items():
            idx2label[i.encoded_labels] = i.label

    label2idx =  {v: k for k, v in idx2label.items()}
    labels_list = list(sorted(label2idx))
    
    return {
        'idx2label': idx2label,
        'label2idx': label2idx,
        'labels_list': labels_list
    }
