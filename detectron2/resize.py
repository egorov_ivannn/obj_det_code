import os
from tqdm import tqdm
import cv2


def resize(old_path, new_path):
    os.makedirs(new_path, exist_ok=True)
    for i in tqdm(os.listdir(old_path)):
        try:
            img = cv2.imread(os.path.join(old_path, i), cv2.IMREAD_COLOR)
            # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            img = cv2.resize(img, (1024, 1024), cv2.INTER_CUBIC)
            cv2.imwrite(os.path.join(new_path, i), img)
        except Exception as e:
            print(e)

# new_path = '/content/drive/MyDrive/japan_detection/test_1024'
# old_path = '/content/drive/MyDrive/japan_detection/test_images/'
# resize(old_path, new_path)

new_path = '/content/drive/MyDrive/japan_detection/train_1024'
old_path = '/content/drive/MyDrive/japan_detection/train_images/'
resize(old_path, new_path)


