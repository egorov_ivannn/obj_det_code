from detectron2.config.config import CfgNode as CN
from detectron2 import model_zoo
from detectron2.config import get_cfg
from detectron2.data import DatasetCatalog, MetadataCatalog
# from detectron2.engine import DefaultPredictor, DefaultTrainer, launch
# from detectron2.evaluation import COCOEvaluator
# from detectron2.structures import BoxMode
# from detectron2.utils.logger import setup_logger
# from detectron2.utils.visualizer import Visualizer
from transforms import MyMapper, AlbumentationsMapper

def make_model_cfg(flags, labels_list):
    cfg = get_cfg()
    # cfg.aug_kwargs = CN(flags.aug_kwargs)  # pass aug_kwargs to cfg
    cfg.aug_kwargs = flags.aug_kwargs  # pass aug_kwargs to cfg

    cfg.OUTPUT_DIR = flags.outdir

    config_name = flags.model_name
    cfg.merge_from_file(model_zoo.get_config_file(config_name))

    cfg.DATASETS.TRAIN = ("fish_train",)
    if flags.split_mode == "all_train":
        cfg.DATASETS.TEST = ()
    else:
        cfg.DATASETS.TEST = ("fish_valid",)
        cfg.TEST.EVAL_PERIOD = flags.eval_period

    cfg.DATALOADER.NUM_WORKERS = flags.num_workers

    cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url(config_name)
    cfg.SOLVER.IMS_PER_BATCH = flags.batch_size
    cfg.SOLVER.LR_SCHEDULER_NAME = flags.lr_scheduler_name
    cfg.SOLVER.BASE_LR = flags.base_lr 
    cfg.SOLVER.MAX_ITER = flags.iter
    cfg.SOLVER.CHECKPOINT_PERIOD = flags.save_every 
    cfg.MODEL.ROI_HEADS.BATCH_SIZE_PER_IMAGE = flags.roi_batch_size_per_image
    cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(labels_list)

    return cfg